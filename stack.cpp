#include "stack.h"
#include <iostream>
int numOfElements = 0;
/*
this function creates the new elemebt and allocates it dynamically
input: the value to be inserted to the stack's new element
output: the newly created element in a stack sruct format
*/
stack* createElement(unsigned int value)
{
	stack* newElement = 0;
	newElement = new stack;
	newElement->value = value;
	return newElement;
}
/*
this function adds a new element to the stack
input: pointer to the pointer of the head of the stack and the element to be inserted
output: none
*/
void push(stack** s, unsigned int element)
{
    int	i = numOfElements - 1;
	stack* temp = *(s);
	stack* newElement = createElement(element);
	if (!*s)
	{
		*s = newElement;
	}
	else
	{
		while (i != 0)
		{
			i--;
			temp = temp->next;
		}
		temp->next = newElement;
		newElement->next = NULL;
	}
	numOfElements++;
}

/*
this function removes the top of the stack
input: the head of the stack
output: the top that has been removed
*/
int pop(stack* s)
{
	int i = numOfElements -1;
	int returner = 0;
	if (s == NULL)
	{
		std::cout << "empty";
		returner = -1;
	}
	else
	{
		while (i != 0)
		{
			s = s->next;
			i--;
		}
		returner = s->value;
	}
	numOfElements--;
	return returner;
}
/*
this function intializes the elements and makes all of them to zeros
input: pointer o the stack's head
output: none 
*/
void initStack(stack* s)
{
	int i = numOfElements - 1;
	while (i != 0)
	{
		s->value = 0;
		s = s->next;
		i--;
	}
	s->value = 0;
}
/*
this function cleans the stack from all the elements
input: pointer to the stack's head
output:none
*/
void cleanStack(stack* s)
{
	int i = numOfElements - 1;
	stack* temp = s->next;
	while (i != 0)
	{
		pop(s);
		s = temp;
		temp = temp->next;
		i--;
	}
	pop(s);
}
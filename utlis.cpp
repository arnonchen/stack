#include "stack.h"
#include "utils.h"
#include <iostream>

#define TEN 10
/*
this function reverses an array
input: the array
output: none
*/
void reverse(int* nums, unsigned int size)
{
	stack* numbers = NULL;
	int i = 0;
	int f = 0;
	while (i != size)
	{
		push(&numbers, nums[i]);
		i++;
	}
	while (f != size)
	{
		nums[f] = pop(numbers);
		f++;
	}
}
/*
this function reverses a ten integers array
input: none
output: the reversed array
*/
int* reverse10()
{
	int i = 0;
	int* nums = new int[10];
	for (i = 0; i < TEN; i++)
	{
		std::cin >> nums[i];
	}
	reverse(nums, TEN);
	return nums;
}
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct linkedList
{
	int value;
	struct linkedList* next;

} linkedList;
linkedList* add(linkedList* head, int value);
linkedList* reduce(linkedList* head);

#endif // LINKEDLIST_H
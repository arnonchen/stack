#include "linkedList.h"
#include <iostream>
/*
this function adds another value to the linked list
input: pointer to the head of the linked list and the value that shall be entered
output: the new linked list with the new value
*/
linkedList* add(linkedList* head, int value)
{
	linkedList* temp = new linkedList;
	temp->value = value;
	temp->next = head;
	head = temp;
	return head;
}
/*
this function removes the top of the linked list
input: pointer to the head of the linked list
output: the new linked list with the new value
*/
linkedList* reduce(linkedList* head)
{
	linkedList* returner = 0;
	if (head == NULL)
	{
		std::cout << "empty";
		returner = head;
	}
	else
	{
		returner = head->next;
	}
	return returner;
}